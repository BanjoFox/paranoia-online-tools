//-
// https://codepen.io/BanjoFox/pen/jOOGppV
//
function main() {
    document.getElementById("results").innerHTML = "NODE is: " + getNODE() + "<br />" + putAdvice()
    + "<br />" + "You Rolled: " +rollNODE() + "<br />" + "Computer Rolled: " + rollCOMPUTER()
    + "<br />" + countSuccess();
}

function getNODE() {
    var node = document.getElementById("inputnode").value;
    return node;
}

function putAdvice() {
    var advice = "";
    if (getNODE() < 0) {
        advice = "Citizen! Please report to R&D for Mandatory UP-Grades";
    } else if (getNODE() === 0) {
        advice = "You seem to require assistance.";
    } else {
        advice = "Great Job! All of Alpha Complex can count on you.";
    }
    return advice;
}

function rollNODE() {
    var counter = Math.abs(getNODE());
    var playerDice = [];
    while (counter > 0) {
        var dice = Math.floor((Math.random() * 6) + 1);
        playerDice.push(dice);
        counter--;
    }

    return playerDice;
}

function rollCOMPUTER() {
    var computerDice = Math.floor((Math.random() * 6) + 1);
    if (computerDice === 6) {
        computerDice = "COMPUTER";
    }
    return computerDice;
}

function countSuccess() {
    var totalDice = rollNODE().concat(rollCOMPUTER());
    var num_success = totalDice.map((i) => i > 4 ? 1 : 0).reduce((total, j) => total + j);
    var num_not_success = totalDice.map((i) => i < 5 ? 1 : 0).reduce((total, j) => total + j);
    //    var success_pl = "es" if num_success > 1 else ""
    //    var not_success_pl = "es" if num_not_success > 1 else ""

    if (rollCOMPUTER() === "COMPUTER") {
		num_success++;
	}

    var outcome = ""
    if (getNODE() < 0) {
        outcome = "Successes: " + (num_not_success - num_success);
    } else {
        outcome = "Successes: " + num_success;
    }
    return outcome;
}
